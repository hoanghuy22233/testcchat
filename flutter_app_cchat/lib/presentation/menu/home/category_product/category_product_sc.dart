// import 'package:flutter_app_cchat/presentation/screen/menu/new/widget_home_page_posts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_cchat/app/constants/barrel_constants.dart';
import 'package:flutter_app_cchat/app/constants/color/color.dart';
import 'package:flutter_app_cchat/presentation/menu/home/category_product/widget_category_product_posts.dart';
import 'package:flutter_app_cchat/presentation/menu/home/home_bloc/bloc.dart';
import 'package:flutter_app_cchat/presentation/menu/home/home_bloc/home_bloc.dart';
import 'package:flutter_app_cchat/presentation/menu/home/home_data_full_bloc/home_data_full_bloc.dart';
import 'package:flutter_app_cchat/presentation/menu/home/home_data_full_bloc/home_data_full_event.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CategoryProductPages extends StatefulWidget {
  CategoryProductPages({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _CategoryProductPages createState() => _CategoryProductPages();
}

class _CategoryProductPages extends State<CategoryProductPages> {
  @override
  void initState() {
    super.initState();
    //  BlocProvider.of<PostBloc>(context).add(LoadPost());
    BlocProvider.of<HomeDataBloc>(context).add(LoadHomeData());
  }

  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
    //   statusBarColor: Colors.red,
    // ));
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: RefreshIndicator(
                onRefresh: () async {
                  BlocProvider.of<HomeDataBloc>(context).add(RefreshHomeData());
                  await Future.delayed(Duration(seconds: 2));
                  return true;
                },
                color: AppColor.PRIMARY_COLOR,
                backgroundColor: AppColor.THIRD_COLOR,
                child: _buildContent()),
          ),
        ],
      ),
    ); // This trailing comma makes auto-formatting nicer for build methods.
  }

  Widget _buildContent() => WidgetPostCategoryProductItem();
}
