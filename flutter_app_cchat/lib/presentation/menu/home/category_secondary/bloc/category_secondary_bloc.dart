// import 'package:flutter_app_cchat/app/constants/barrel_constants.dart';
// import 'package:flutter_app_cchat/app/constants/endpoint/app_anh_quan_endpoint.dart';
// import 'package:flutter_app_cchat/model/entity/barrel_entity.dart';
// import 'package:flutter_app_cchat/model/entity/category_product_child.dart';
// import 'package:flutter_app_cchat/model/entity/category_product_hot.dart';
// import 'package:flutter_app_cchat/model/entity/color_catergory_product_hot.dart';
// import 'package:flutter_app_cchat/model/entity/sizes.dart';
// import 'package:flutter_app_cchat/model/repo/barrel_repo.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:meta/meta.dart';
//
// import 'category_secondary_two_event.dart';
// import 'category_secondary_two_state.dart';
//
// class CategorySecondaryBloc
//     extends Bloc<CategorySecondaryEvent, CategorySecondaryState> {
//   final HomeRepository homeRepository;
//
//   CategorySecondaryBloc({@required this.homeRepository});
//
//   @override
//   CategorySecondaryState get initialState => CategorySecondaryLoading();
//
//   @override
//   Stream<CategorySecondaryState> mapEventToState(
//       CategorySecondaryEvent event) async* {
//     if (event is FilterCategorySecondary) {
//       yield CategorySecondaryLoading();
//       print('event is FilterCategorySecondary');
//       print('event.priceBegin ${event.priceBegin}');
//       print('event.priceEnd ${event.priceEnd}');
//       yield* _mapLoadingState(event.category, event.size, event.color,
//           event.priceBegin, event.priceEnd);
//     }
//     if (event is ChangeCategorySecondary) {
//       yield CategorySecondaryLoading();
//       yield* _mapLoadingState(event.category, event.size, event.color,
//           event.priceBegin, event.priceEnd);
//     }
//     if (event is LoadCategorySecondary) {
// //      yield CategorySecondaryLoading();
//       yield* _mapLoadingState(event.category, event.size, event.color,
//           event.priceBegin, event.priceEnd);
//     } else if (event is RefreshCategorySecondary) {
//       yield CategorySecondaryLoading();
//       yield* _mapLoadingState(event.category, event.size, event.color,
//           event.priceBegin, event.priceEnd);
//     }
//   }
//
//   Stream<CategorySecondaryState> _mapLoadingState(
//       CategoryProductChild category,
//       Sizes size,
//       ColorCatergoryProductHot color,
//       double priceBegin,
//       double priceEnd) async* {
//     final currentState = state;
//     try {
//       if (state is CategorySecondaryLoading) {
//         final products = await _fetchProduct(
//             limit: Endpoint.DEFAULT_LIMIT,
//             offset: 0,
//             categoryId: category.id,
//             colorId: color != null ? color.id : null,
//             sizeId: size != null ? size.id : null,
//             priceBegin: priceBegin,
//             priceEnd: priceEnd);
//         yield CategorySecondaryLoaded(
//             products: products, category: category, hasReachedMax: false);
//       }
//       if (currentState is CategorySecondaryLoaded) {
//         final products = await _fetchProduct(
//             limit: Endpoint.DEFAULT_LIMIT,
//             offset: currentState.products.length,
//             categoryId: category.id,
//             colorId: color != null ? color.id : null,
//             sizeId: size != null ? size.id : null,
//             priceBegin: priceBegin,
//             priceEnd: priceEnd);
//         yield products.isEmpty
//             ? currentState.copyWith(hasReachedMax: true)
//             : CategorySecondaryLoaded(
//                 products: currentState.products + products,
//                 hasReachedMax: false,
//               );
//       }
//     } catch (e) {
//       print(e);
//       yield CategorySecondaryNotLoaded('$e');
//     }
//   }
//
//
//   Future<List<CategoryProductHotData>> _fetchProduct(
//       {@required int limit,
//       @required int offset,
//       @required int categoryId,
//       int sizeId,
//       int colorId,
//       double priceBegin,
//       double priceEnd}) async {
//     try {
//       final response = await homeRepository.getProductByAttr(
//           limit: limit,
//           offset: offset,
//           categoryId: categoryId,
//           sizeId: sizeId,
//           colorId: colorId,
//           priceBegin: priceBegin,
//           priceEnd: priceEnd);
//       return response.data.list;
//     } catch (e) {
//       throw Exception('$e');
//     }
//   }
// }
