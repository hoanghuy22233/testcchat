// import 'package:flutter_app_cchat/model/entity/barrel_entity.dart';
// import 'package:equatable/equatable.dart';
// import 'package:flutter_app_cchat/model/entity/category_product_child.dart';
// import 'package:flutter_app_cchat/model/entity/category_product_hot.dart';
//
// abstract class CategorySecondaryState extends Equatable {
//   const CategorySecondaryState();
//
//   @override
//   List<Object> get props => [];
// }
//
// class CategorySecondaryLoading extends CategorySecondaryState {}
//
// class CategorySecondaryLoaded extends CategorySecondaryState {
//   final List<CategoryProductHotData> products;
//   final CategoryProductChild category;
//   final bool hasReachedMax;
//
//   const CategorySecondaryLoaded({this.products, this.category, this.hasReachedMax});
//
//   CategorySecondaryLoaded copyWith({
//     List<CategoryProductHotData> products,
//     CategoryProductChild category,
//     bool hasReachedMax,
//   }) {
//     return CategorySecondaryLoaded(
//       products: products ?? this.products,
//       category: category ?? this.category,
//       hasReachedMax: hasReachedMax ?? this.hasReachedMax,
//     );
//   }
//
//   @override
//   List<Object> get props => [products, category, hasReachedMax];
//
//   @override
//   String toString() {
//     return 'CategorySecondaryLoaded{products: $products, category: $category, hasReachedMax: $hasReachedMax}';
//   }
// }
//
// class CategorySecondaryNotLoaded extends CategorySecondaryState {
//   final String error;
//
//   CategorySecondaryNotLoaded(this.error);
//
//   @override
//   String toString() {
//     return 'CategorySecondaryNotLoaded{error: $error}';
//   }
// }