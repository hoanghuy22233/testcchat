// import 'package:flutter_app_cchat/model/entity/barrel_entity.dart';
// import 'package:equatable/equatable.dart';
// import 'package:flutter_app_cchat/model/entity/category_product_child.dart';
// import 'package:flutter_app_cchat/model/entity/color_catergory_product_hot.dart';
// import 'package:flutter_app_cchat/model/entity/sizes.dart';
//
// class CategorySecondaryEvent extends Equatable {
//   const CategorySecondaryEvent();
//
//   List<Object> get props => [];
// }
//
// class ChangeCategorySecondary extends CategorySecondaryEvent {
//   final CategoryProductChild category;
//   final Sizes size;
//   final ColorCatergoryProductHot color;
//   final double priceBegin;
//   final double priceEnd;
//
//   ChangeCategorySecondary(
//       this.category, this.size, this.color, this.priceBegin, this.priceEnd);
//
//   @override
//   List<Object> get props => [category, size, color, priceBegin, priceEnd];
//
//   @override
//   String toString() {
//     return 'ChangeCategorySecondary{category: $category, size: $size, color: $color, priceBegin: $priceBegin, priceEnd: $priceEnd}';
//   }
// }
//
// class FilterCategorySecondary extends CategorySecondaryEvent {
//   final CategoryProductChild category;
//   final Sizes size;
//   final ColorCatergoryProductHot color;
//   final double priceBegin;
//   final double priceEnd;
//
//   FilterCategorySecondary(
//       this.category, this.size, this.color, this.priceBegin, this.priceEnd);
//
//   @override
//   List<Object> get props => [category, size, color, priceBegin, priceEnd];
//
//   @override
//   String toString() {
//     return 'FilterCategorySecondary{category: $category, size: $size, color: $color, priceBegin: $priceBegin, priceEnd: $priceEnd}';
//   }
// }
//
// class LoadCategorySecondary extends CategorySecondaryEvent {
//   final CategoryProductChild category;
//   final Sizes size;
//   final ColorCatergoryProductHot color;
//   final double priceBegin;
//   final double priceEnd;
//
//   LoadCategorySecondary(
//       this.category, this.size, this.color, this.priceBegin, this.priceEnd);
//
//   @override
//   List<Object> get props => [category, size, color, priceBegin, priceEnd];
//
//   @override
//   String toString() {
//     return 'LoadCategorySecondary{category: $category, size: $size, color: $color, priceBegin: $priceBegin, priceEnd: $priceEnd}';
//   }
// }
//
// class RefreshCategorySecondary extends CategorySecondaryEvent {
//   final CategoryProductChild category;
//   final Sizes size;
//   final ColorCatergoryProductHot color;
//   final double priceBegin;
//   final double priceEnd;
//
//   RefreshCategorySecondary(
//       this.category, this.size, this.color, this.priceBegin, this.priceEnd);
//
//   @override
//   List<Object> get props => [category, size, color, priceBegin, priceEnd];
//
//   @override
//   String toString() {
//     return 'RefreshCategorySecondary{category: $category, size: $size, color: $color, priceBegin: $priceBegin, priceEnd: $priceEnd}';
//   }
// }
