// import 'package:flutter_app_cchat/app/constants/barrel_constants.dart';
// import 'package:flutter_app_cchat/model/entity/barrel_entity.dart';
// import 'package:flutter_app_cchat/model/entity/category_product_child.dart';
// import 'package:flutter_app_cchat/model/entity/color_catergory_product_hot.dart';
// import 'package:flutter_app_cchat/model/entity/sizes.dart';
// import 'package:flutter_app_cchat/presentation/common_widgets/barrel_common_widgets.dart';
// import 'package:flutter_app_cchat/presentation/common_widgets/widget_appbar_main.dart';
// import 'package:flutter_app_cchat/presentation/common_widgets/widget_product_grid.dart';
// import 'package:flutter_app_cchat/presentation/menu/home/category_product/bloc/post_category_product_bloc.dart';
// import 'package:flutter_app_cchat/presentation/menu/home/category_product/bloc/post_category_product_state.dart';
// import 'package:flutter_app_cchat/utils/handler/http_handler.dart';
// import 'package:flutter_app_cchat/utils/locale/app_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:get/get.dart';
// import 'package:pull_to_refresh/pull_to_refresh.dart';
// import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
//
// import 'bloc/category_secondary_two_bloc.dart';
// import 'bloc/category_secondary_two_event.dart';
// import 'bloc/category_secondary_two_state.dart';
//
// class CategorySecondaryScreen extends StatefulWidget {
//   @override
//   _CategorySecondaryScreenState createState() =>
//       _CategorySecondaryScreenState();
// }
//
// class _CategorySecondaryScreenState extends State<CategorySecondaryScreen> {
//   CategoryProductChild _category;
//   ColorCatergoryProductHot _color;
//   Sizes _size;
//   double _priceBegin = AppValue.PRODUCT_FILTER_PRICE_BEGIN;
//   double _priceEnd = AppValue.PRODUCT_FILTER_PRICE_END;
//
//   ItemScrollController _primaryScrollController = ItemScrollController();
//   RefreshController _refreshController =
//       RefreshController(initialRefresh: false);
//
//   GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
//
//   _onRefresh() async {
//     BlocProvider.of<CategorySecondaryBloc>(context).add(
//         RefreshCategorySecondary(
//             _category, _size, _color, _priceBegin, _priceEnd));
//   }
//
//   _onLoadMore() async {
//     BlocProvider.of<CategorySecondaryBloc>(context).add(LoadCategorySecondary(
//         _category, _size, _color, _priceBegin, _priceEnd));
//   }
//
//   _onArgument() {
//     Future.delayed(Duration.zero, () async {
//       final Map arguments = ModalRoute.of(context).settings.arguments as Map;
//       setState(() {
//         _category = arguments['category'];
//         print('---_category---');
//         print(_category);
//
//         BlocProvider.of<CategorySecondaryBloc>(context).add(
//             LoadCategorySecondary(
//                 _category, _size, _color, _priceBegin, _priceEnd));
//       });
//     });
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     _onArgument();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//         child: BlocListener<PostCategoryProductBloc, PostCategoryProductState>(
//           listener: (context, state) async {
//             if (state.isLoading) {
//               await HttpHandler.resolve(status: state.status);
//             }
//
//             if (state.isSuccess) {
//               await HttpHandler.resolve(status: state.status);
//               print("_____________");
//               print(state.status);
//             }
//
//             if (state.isFailure) {
//               await HttpHandler.resolve(status: state.status);
//               print(state.status);
//             }
//           },
//             child: BlocBuilder<PostCategoryProductBloc, PostCategoryProductState>(
//               builder: (context, state) {
//                 return Scaffold(
//                   key: _drawerKey,
//                   backgroundColor: Colors.white,
//                   body: Container(
//                     child: Column(
//                       children: [_buildAppbar(), Expanded(child: _buildContent())],
//                     ),
//                   ),
//                 );
//               },
//             )
//         ));
//
//   }
//
// //
//
//   Widget _buildContent() {
//     return BlocListener<CategorySecondaryBloc, CategorySecondaryState>(
//       listener: (context, state) {
//         if (state is CategorySecondaryLoaded) {
//           if (state.hasReachedMax) {
//             _refreshController.loadNoData();
//           } else {
//             _refreshController.loadComplete();
//           }
//         }
//       },
//       child: BlocBuilder<CategorySecondaryBloc, CategorySecondaryState>(
//           builder: (context, state) {
//         if (state is CategorySecondaryLoaded) {
//           return Column(
//             children: [_buildMenuBar(), Expanded(child: _buildProduct(state))],
//           );
//         } else if (state is CategorySecondaryLoading) {
//           return Center(
//             child: WidgetCircleProgress(),
//           );
//         } else if (state is CategorySecondaryNotLoaded) {
//           return Center(
//             child: Text('${state.error}'),
//           );
//         } else {
//           return Center(
//             child: Text('Unknown state'),
//           );
//         }
//       }),
//     );
//   }
//
//   static final corner = 10.0;
//   static final noCorner = corner / 2;
//   static final paddingEven = EdgeInsets.only(
//       left: corner, right: noCorner, top: noCorner, bottom: noCorner);
//   static final paddingOdd = EdgeInsets.only(
//       left: noCorner, right: corner, top: noCorner, bottom: noCorner);
//
//   Widget _buildProduct(CategorySecondaryLoaded state) {
//     return Container(
//       child: WidgetRefresher(
//         refreshController: _refreshController,
//         scrollDirection: Axis.vertical,
//         onLoading: _onLoadMore,
//         onRefresh: _onRefresh,
//         child: WidgetWrapBuilder.build(
//             itemBuilder: _itemBuilder, data: state.products),
//       ),
//     );
//   }
//
//   Widget _itemBuilder(list, context, index) {
//     if(list == null) return WidgetProductGrid(empty: true,);
//     return Container(
//       child: Padding(
//         padding: index % 2 == 0 ? paddingEven : paddingOdd,
//         child: WidgetProductGrid(
//           product: list[index],
//           full: (index + 1) % 3 == 0,
//         ),
//       ),
//     );
//   }
//
//   Widget _buildMenuBar() {
//     return Container(
//       padding:
//           EdgeInsets.symmetric(horizontal: AppValue.APP_HORIZONTAL_PADDING),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           Row(
//             children: [
//               Container(
//                   width: AppValue.ACTION_BAR_HEIGHT,
//                   height: AppValue.ACTION_BAR_HEIGHT,
//                   child: WidgetAppbarMenu(
//                     icon: Image.asset('assets/images/img_category.png'),
//                     onTap: () {
//                       _drawerKey.currentState.openDrawer();
//                     },
//                   )),
//               Text(
//                 AppLocalizations.of(context)
//                     .translate('category_secondary.category'),
//                 style: AppStyle.DEFAULT_MEDIUM,
//               )
//             ],
//           ),
//           Row(
//             children: [
//               Container(
//                 width: AppValue.ACTION_BAR_HEIGHT,
//                 height: AppValue.ACTION_BAR_HEIGHT,
//                 alignment: Alignment.centerRight,
//                 child: Container(
//                   width: AppValue.ACTION_BAR_HEIGHT * 0.85,
//                   height: AppValue.ACTION_BAR_HEIGHT * 0.85,
//                   child: WidgetAppbarMenu(
//                     icon: Image.asset('assets/images/img_filter.png'),
//                     onTap: () {
//                       _drawerKey.currentState.openEndDrawer();
//                     },
//                   ),
//                 ),
//               ),
//               Text(
//                 AppLocalizations.of(context)
//                     .translate('category_secondary.filter'),
//                 style: AppStyle.DEFAULT_MEDIUM,
//               )
//             ],
//           )
//         ],
//       ),
//     );
//   }
//
//   Widget _buildAppbar() => WidgetAppBarMain(
//         actionBack: () {
//           AppNavigator.navigateBack();
//         },
//         canSearch: false,
//         title:
//             '${AppLocalizations.of(context).translate('category_primary.title')} ${_category?.name ?? ''}',
//       );
// }
