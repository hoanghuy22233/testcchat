import 'package:flutter_app_cchat/model/repo/barrel_repo.dart';
import 'package:flutter_app_cchat/utils/utils.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';

import 'home_data_full_event.dart';
import 'home_data_full_state.dart';

class HomeDataFullBloc extends Bloc<HomeDataFullEvent, HomeDataFullState> {
  final HomeRepository homeRepository;
  // final UserRepository userRepository;

  // HomeDataFullBloc({@required this.homeRepository, this.userRepository});
  HomeDataFullBloc({@required this.homeRepository});


  @override
  HomeDataFullState get initialState => HomeDataFullLoading();

  @override
  Stream<HomeDataFullState> mapEventToState(HomeDataFullEvent event) async* {
    if (event is LoadHome) {
      yield* _mapLoadHomeToState();
    } else if (event is RefreshHome) {
      yield HomeDataFullLoading();
      yield* _mapLoadHomeToState();
    }
  }

  Stream<HomeDataFullState> _mapLoadHomeToState() async* {
    try {
      final homeResponse = await homeRepository.getHomeData();
      // final appConfigResponse = await userRepository.getAppConfigs();
      print('-----------------');
      print(homeResponse);
      // print(appConfigResponse);
      await Future.delayed(Duration(milliseconds: 500), () {});
      // yield HomeLoaded(homeResponse: homeResponse, appConfigResponse: appConfigResponse);
      yield HomeDataFullLoaded(homeResponse: homeResponse);

    } catch (e) {
      yield HomeDataFullNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }
}
