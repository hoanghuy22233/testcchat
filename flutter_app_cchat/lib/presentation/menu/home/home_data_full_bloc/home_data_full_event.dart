import 'package:equatable/equatable.dart';

class HomeDataFullEvent extends Equatable {
  const HomeDataFullEvent();

  List<Object> get props => [];
}

class LoadHome extends HomeDataFullEvent {
}

class RefreshHome extends HomeDataFullEvent {}
