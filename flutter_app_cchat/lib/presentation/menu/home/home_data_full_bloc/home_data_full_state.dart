import 'package:flutter_app_cchat/model/api/response/barrel_response.dart';
import 'package:flutter_app_cchat/model/api/response/home_response.dart';
import 'package:flutter_app_cchat/utils/utils.dart';
import 'package:equatable/equatable.dart';

abstract class HomeDataFullState extends Equatable {
  const HomeDataFullState();

  @override
  List<Object> get props => [];
}

class HomeDataFullLoading extends HomeDataFullState {}

class HomeDataFullLoaded extends HomeDataFullState {
  final HomeResponse homeResponse;
  // final AppConfigResponse appConfigResponse;

  // const HomeLoaded({this.homeResponse, this.appConfigResponse});
  const HomeDataFullLoaded({this.homeResponse});


  @override
  // List<Object> get props => [homeResponse, appConfigResponse];
  List<Object> get props => [homeResponse];


  @override
  String toString() {
    // return 'HomeLoaded{homeResponse: $homeResponse, appConfigResponse: $appConfigResponse}';
    return 'HomeLoaded{homeResponse: $homeResponse,}';

  }


}

class HomeDataFullNotLoaded extends HomeDataFullState {
  final DioStatus status;

  HomeDataFullNotLoaded({this.status});

  @override
  String toString() {
    return 'HomeNotLoaded{error: $status}';
  }
}